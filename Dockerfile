FROM openjdk:8-jdk-alpine
RUN apk --no-cache add curl
EXPOSE 8080
ADD target/rateit-0.0.1-SNAPSHOT.jar rateit-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","-Dspring.profiles.active=docker", "rateit-0.0.1-SNAPSHOT.jar"]

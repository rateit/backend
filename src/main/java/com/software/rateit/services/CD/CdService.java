package com.software.rateit.services.CD;

import com.querydsl.core.types.Predicate;
import com.software.rateit.DTO.CD.*;
import com.software.rateit.DTO.Track.NewTrackDTO;
import com.software.rateit.DTO.Track.TrackDTO;
import com.software.rateit.Entity.OwnedCd;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.multipart.MultipartFile;


public interface CdService {
    ResponseEntity<CDWrapper> findAllAlbums(Pageable pageable, Predicate predicate);
    ResponseEntity<Iterable<CdDTO>> getRatingList();
    ResponseEntity<CDWithCommentsDTO> findOneAlbum(long id);
    ResponseEntity<CdDTO> updateAlbum(long id, CdDTO cdDTO);
    ResponseEntity<Void> deleteAlbum(long id);
    ResponseEntity<CdDTO> addNewAlbum(CdDTO cd );
    ResponseEntity<CdDTO> rateCD(long id, long userId, float note);
    ResponseEntity<CdDTO> addTrackToAlbum(long cdId, NewTrackDTO newTrackDTO);
    ResponseEntity<Iterable<TrackDTO>> getCdTracks(long id);
    ResponseEntity<CdDTO> addCdCover(MultipartFile file, long id);
    ResponseEntity<OwnedCdWrapper> getAllExistingAlbums(Pageable pageable, Predicate predicate);
    ResponseEntity<OwnedCdDTO> getOneExistingAlbum(long id);
    ResponseEntity<OwnedCdDTO> addImageOfExistingAlbum(Authentication authentication, MultipartFile file, long id);
    ResponseEntity<Void> removeImageOfExistingAlbum(Authentication authentication, long photoId, long cdId);
}

package com.software.rateit.services.Email;

import com.software.rateit.DTO.Email.EmailInfo;
import com.software.rateit.Entity.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import javax.jms.Queue;
import java.util.HashMap;
import java.util.Map;

@Service
public class EmailServiceImpl implements EmailService{

    private static final Logger LOG = LoggerFactory.getLogger(EmailServiceImpl.class);

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private Queue queue;

    @Override
    public void simpleEmail(EmailInfo email) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(email.getTo());
        simpleMailMessage.setSubject(email.getSubject());
        simpleMailMessage.setText(email.getContent());
        simpleMailMessage.setFrom("doNotResponse@rateit.com");
        try {
            javaMailSender.send(simpleMailMessage);
        } catch (MailException e) {
            LOG.error("Error while sending out email..{}", e.getMessage());
            return;
        }
        LOG.info("Email to {} sent successfully", email.getTo());

    }

    @Override
    public ResponseEntity<String> sendSimpleEmail(EmailInfo email) {
        addToQueue(email);
        return new ResponseEntity<>("Email sent", HttpStatus.OK);
    }

    @Override
    public void sendNewMessageNotification(String emailTo, String message, String from) {
        EmailInfo emailInfo = new EmailInfo();
        emailInfo.setTo(emailTo);
        emailInfo.setSubject(from + " has send you a message");
        emailInfo.setContent("Hello!\n\nYou have received a new message from " + from + ":\n\n" + message + "\n\nReply now:\n<link>\n\nSincerely,\nRateIt team");

        addToQueue(emailInfo);
    }

    @Override
    public void sendFriendRequestNotification(String emailTo, String from) {
        EmailInfo emailInfo = new EmailInfo();
        emailInfo.setTo(emailTo);
        emailInfo.setSubject("You have received new friend request!");
        emailInfo.setContent("Hello! \n\n" + from + "wants to be your friend. You can confirm it now! \n\n <link> \n\nSincerely,\nRateIt team");

        addToQueue(emailInfo);
    }

    @Override
    public void sendFriendHasYourWishedCdNotification(String emailTo, String from, String title, Type type) {

        EmailInfo emailInfo = new EmailInfo();
        emailInfo.setTo(emailTo);
        emailInfo.setSubject("Your friend has an item from your wishlist!");
        emailInfo.setContent("Hello! \n\n" + from + "just added a " + type +"\""+ title + "\"! You can contact him now! \n\n <link> \n\nSincerely,\nRateIt team");

        addToQueue(emailInfo);
    }

    private void addToQueue(EmailInfo emailInfo){
        Map<String,String> mapEmail = new HashMap<>();
        mapEmail.put("to", emailInfo.getTo());
        mapEmail.put("subject", emailInfo.getSubject());
        mapEmail.put("message", emailInfo.getContent());
        jmsTemplate.convertAndSend(queue, mapEmail);
    }


}

package com.software.rateit.services.Email;

import com.software.rateit.DTO.Email.EmailInfo;
import com.software.rateit.Entity.Type;
import org.springframework.http.ResponseEntity;

public interface EmailService {
    void simpleEmail(EmailInfo email);
    ResponseEntity<String> sendSimpleEmail(EmailInfo email);
    void sendNewMessageNotification(String emailTo, String message, String from);
    void sendFriendRequestNotification(String emailTo, String from);
    void sendFriendHasYourWishedCdNotification(String emailTo, String from, String title, Type type);

}

package com.software.rateit.services.Chat;

import com.software.rateit.DTO.Chat.ChatDTO;
import com.software.rateit.DTO.Chat.NewMessageDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface ChatService {

    ResponseEntity<List<ChatDTO>> getMyChats(Authentication authentication);
    ResponseEntity<ChatDTO> newChat(Authentication authentication, long secondPersonId);
    ResponseEntity<ChatDTO> getOneChat(Authentication authentication, long secondPersonId);
    ResponseEntity<ChatDTO> writeNewMessage(Authentication authentication, NewMessageDTO newMessageDTO);
    ResponseEntity<ChatDTO> checkMessageAsSeen(Authentication authentication, long secondPersonId);

}

package com.software.rateit.services.Chat;

import com.software.rateit.DTO.Chat.ChatDTO;
import com.software.rateit.DTO.Chat.MessageDTO;
import com.software.rateit.DTO.Chat.NewMessageDTO;
import com.software.rateit.DTO.User.UserMapper;
import com.software.rateit.Entity.Chat;
import com.software.rateit.Entity.Message;
import com.software.rateit.Entity.User;
import com.software.rateit.exceptions.ForbiddenException;
import com.software.rateit.exceptions.NotFoundException;
import com.software.rateit.repositories.ChatRepository;
import com.software.rateit.repositories.MessageRepository;
import com.software.rateit.repositories.UserRepository;
import com.software.rateit.services.Email.EmailService;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class ChatServiceImpl implements ChatService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private EmailService emailService;

    private UserMapper userMapper = Mappers.getMapper(UserMapper.class);

    @Override
    public ResponseEntity<List<ChatDTO>> getMyChats(Authentication authentication) {
        User user = userRepository.findOneByNickIgnoreCase(authentication.getPrincipal().toString());
        Set<Chat> chats = chatRepository.findAllByParticipant1OrParticipant2(user, user);

        List<ChatDTO> response = new ArrayList<>();

        chats.forEach(chat -> {
            ChatDTO chatDTO = new ChatDTO();
            chatDTO.setId(chat.getId());
            List<MessageDTO> messages = new ArrayList<>();

            chat.getMessages().forEach(message -> {
                MessageDTO messageDTO = new MessageDTO();
                messageDTO.setId(message.getId());
                messageDTO.setMessage(message.getMessage());
                messageDTO.setSeen(message.isSeen());
                messageDTO.setTime(message.getTime());
                if(message.getSenderId() == chat.getParticipant1().getId())
                    messageDTO.setSender(userMapper.mapToUserDTO(chat.getParticipant1()));
                else if(message.getSenderId() == chat.getParticipant2().getId())
                    messageDTO.setSender(userMapper.mapToUserDTO(chat.getParticipant2()));
                messages.add(messageDTO);
            });

            chatDTO.setMessages(messages);
            if(chat.getParticipant1().equals(user)){
                chatDTO.setParticipant(userMapper.mapToUserDTO(chat.getParticipant2()));
                chatDTO.setMe(userMapper.mapToUserDTO(chat.getParticipant1()));
            } else if(chat.getParticipant2().equals(user)) {
                chatDTO.setMe(userMapper.mapToUserDTO(chat.getParticipant2()));
                chatDTO.setParticipant(userMapper.mapToUserDTO(chat.getParticipant1()));
            }
            response.add(chatDTO);
        });

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ChatDTO> newChat(Authentication authentication, long secondPersonId) {
        User me = userRepository.findOneByNickIgnoreCase(authentication.getPrincipal().toString());
        User friend = userRepository.findOneById(secondPersonId);

        if(chatRepository.findOneByParticipant1AndParticipant2(me, friend) != null || chatRepository.findOneByParticipant1AndParticipant2(friend, me) != null) {
            throw new ForbiddenException("Chat already exists");
        }

        Chat chat = new Chat();
        chat.setParticipant1(me);
        chat.setParticipant2(friend);
        Chat response = chatRepository.save(chat);

        ChatDTO chatDTO = new ChatDTO();
        chatDTO.setMe(userMapper.mapToUserDTO(response.getParticipant1()));
        chatDTO.setParticipant(userMapper.mapToUserDTO(response.getParticipant2()));

        return new ResponseEntity<>(chatDTO, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ChatDTO> getOneChat(Authentication authentication, long secondPersonId) {

        User me = userRepository.findOneByNickIgnoreCase(authentication.getPrincipal().toString());
        User friend = userRepository.findOneById(secondPersonId);

        Chat meAsPerson1 = chatRepository.findOneByParticipant1AndParticipant2(me, friend);
        Chat meAsPerson2 = chatRepository.findOneByParticipant1AndParticipant2(friend, me);

        ChatDTO chatDTO = new ChatDTO();
        List<MessageDTO> messageDTOS = new ArrayList<>();

        if(meAsPerson1 == null && meAsPerson2 != null) {
            chatDTO.setMe(userMapper.mapToUserDTO(meAsPerson2.getParticipant2()));
            chatDTO.setParticipant(userMapper.mapToUserDTO(meAsPerson2.getParticipant1()));
            chatDTO.setId(meAsPerson2.getId());
            meAsPerson2.getMessages().forEach(message -> {
                MessageDTO messageDTO = new MessageDTO();
                messageDTO.setTime(message.getTime());
                messageDTO.setSeen(message.isSeen());
                messageDTO.setMessage(message.getMessage());
                messageDTO.setId(message.getId());
                if(message.getSenderId() == meAsPerson2.getParticipant1().getId())
                    messageDTO.setSender(userMapper.mapToUserDTO(meAsPerson2.getParticipant1()));
                else if(message.getSenderId() == meAsPerson2.getParticipant2().getId())
                    messageDTO.setSender(userMapper.mapToUserDTO(meAsPerson2.getParticipant2()));
                messageDTOS.add(messageDTO);
            });
            chatDTO.setMessages(messageDTOS);
        } else if (meAsPerson1 != null && meAsPerson2 == null) {
            chatDTO.setMe(userMapper.mapToUserDTO(meAsPerson1.getParticipant1()));
            chatDTO.setParticipant(userMapper.mapToUserDTO(meAsPerson1.getParticipant2()));
            chatDTO.setId(meAsPerson1.getId());
            meAsPerson1.getMessages().forEach(message -> {
                MessageDTO messageDTO = new MessageDTO();
                messageDTO.setTime(message.getTime());
                messageDTO.setSeen(message.isSeen());
                messageDTO.setMessage(message.getMessage());
                messageDTO.setId(message.getId());
                if(message.getSenderId() == meAsPerson1.getParticipant1().getId())
                    messageDTO.setSender(userMapper.mapToUserDTO(meAsPerson1.getParticipant1()));
                else if(message.getSenderId() == meAsPerson1.getParticipant2().getId())
                    messageDTO.setSender(userMapper.mapToUserDTO(meAsPerson1.getParticipant2()));
                messageDTOS.add(messageDTO);
            });
            chatDTO.setMessages(messageDTOS);
        } else {
            throw new ForbiddenException("Something went wrong");
        }

        return new ResponseEntity<>(chatDTO, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ChatDTO> writeNewMessage(Authentication authentication, NewMessageDTO newMessageDTO) {

        if(newMessageDTO.getReceiverId() == null){
            throw new NotFoundException("No receiver id");
        }
        User me = userRepository.findOneByNickIgnoreCase(authentication.getPrincipal().toString());
        User friend = userRepository.findOneById(newMessageDTO.getReceiverId());

        if(friend == null) {
            throw new NotFoundException("Wrong receiverId");
        }

        Chat chat = chatRepository.findOneByParticipant1AndParticipant2(me, friend);
        Chat chatReverse = chatRepository.findOneByParticipant1AndParticipant2(friend, me);

        Message message = new Message();
        message.setMessage(newMessageDTO.getMessage());
        message.setSenderId(me.getId());
        messageRepository.save(message);
        List<MessageDTO> messageDTOS = new ArrayList<>();
        ChatDTO chatDTO = new ChatDTO();

        if(chat == null && chatReverse != null) {
            chatReverse.addMessage(message);
            chatDTO.setMe(userMapper.mapToUserDTO(me));
            chatDTO.setParticipant(userMapper.mapToUserDTO(friend));
            chatDTO.setId(chatReverse.getId());
            chatReverse.getMessages().forEach(msg -> {
                MessageDTO messageDTO = new MessageDTO();
                messageDTO.setSender(userMapper.mapToUserDTO(userRepository.findOneById(message.getSenderId())));
                messageDTO.setId(message.getId());
                messageDTO.setMessage(message.getMessage());
                messageDTO.setTime(message.getTime());
                messageDTO.setSeen(message.isSeen());
                messageDTOS.add(messageDTO);
            });
            chatRepository.save(chatReverse);

        } else if (chat != null && chatReverse == null) {
            chat.addMessage(message);
            chatDTO.setMe(userMapper.mapToUserDTO(me));
            chatDTO.setParticipant(userMapper.mapToUserDTO(friend));
            chatDTO.setId(chat.getId());
            chat.getMessages().forEach(msg -> {
                MessageDTO messageDTO = new MessageDTO();
                messageDTO.setSender(userMapper.mapToUserDTO(userRepository.findOneById(message.getSenderId())));
                messageDTO.setId(message.getId());
                messageDTO.setMessage(message.getMessage());
                messageDTO.setTime(message.getTime());
                messageDTO.setSeen(message.isSeen());
                messageDTOS.add(messageDTO);
            });
            chatRepository.save(chat);
        } else {
            throw new NotFoundException("Chat doesnt exists");
        }

        if(!friend.isActive()){
            emailService.sendNewMessageNotification(friend.getEmail(), newMessageDTO.getMessage(), me.getNick());
        }

        chatDTO.setMessages(messageDTOS);
        return new ResponseEntity<>(chatDTO, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ChatDTO> checkMessageAsSeen(Authentication authentication, long secondPersonId) {
        User me = userRepository.findOneByNickIgnoreCase(authentication.getPrincipal().toString());
        User friend = userRepository.findOneById(secondPersonId);

        Chat chat = chatRepository.findOneByParticipant1AndParticipant2(me, friend);
        Chat chatReverse = chatRepository.findOneByParticipant1AndParticipant2(friend, me);

        ChatDTO chatDTO = new ChatDTO();
        List<MessageDTO> messageDTOS = new ArrayList<>();

        if(chat == null && chatReverse != null) {
            chatReverse.getMessages().forEach(msg -> {
                checkIfMessageIsNotYours(msg, me.getId(), friend.getId());
            });
            chatRepository.save(chatReverse);
            chatDTO.setMe(userMapper.mapToUserDTO(me));
            chatDTO.setParticipant(userMapper.mapToUserDTO(friend));
            chatDTO.setId(chatReverse.getId());
            chatReverse.getMessages().forEach(msg -> {
                MessageDTO messageDTO = new MessageDTO();
                messageDTO.setSender(userMapper.mapToUserDTO(userRepository.findOneById(msg.getSenderId())));
                messageDTO.setId(msg.getId());
                messageDTO.setMessage(msg.getMessage());
                messageDTO.setTime(msg.getTime());
                messageDTO.setSeen(msg.isSeen());
                messageDTOS.add(messageDTO);
            });
            chatDTO.setMessages(messageDTOS);
        } else if (chat != null && chatReverse == null) {
            chat.getMessages().forEach(msg -> {
                checkIfMessageIsNotYours(msg, me.getId(), friend.getId());
            });
            chatRepository.save(chat);
            chatDTO.setMe(userMapper.mapToUserDTO(me));
            chatDTO.setParticipant(userMapper.mapToUserDTO(friend));
            chatDTO.setId(chat.getId());
            chat.getMessages().forEach(msg -> {
                MessageDTO messageDTO = new MessageDTO();
                messageDTO.setSender(userMapper.mapToUserDTO(userRepository.findOneById(msg.getSenderId())));
                messageDTO.setId(msg.getId());
                messageDTO.setMessage(msg.getMessage());
                messageDTO.setTime(msg.getTime());
                messageDTO.setSeen(msg.isSeen());
                messageDTOS.add(messageDTO);
            });
            chatDTO.setMessages(messageDTOS);
        } else {
            throw new NotFoundException("Chat doesn't exists");
        }

        return new ResponseEntity<>(chatDTO, HttpStatus.OK);
    }

    private void checkIfMessageIsNotYours(Message message, long myId, long hisId){
        if(message.getSenderId() == myId) {

        } else if(message.getSenderId() == hisId) {
            message.setSeen(true);
        } else {
            throw new ForbiddenException("Something went wrong");
        }
    }
}

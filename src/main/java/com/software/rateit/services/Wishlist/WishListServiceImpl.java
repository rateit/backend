package com.software.rateit.services.Wishlist;

import com.software.rateit.DTO.CD.OwnedCdDTO;
import com.software.rateit.DTO.CD.OwnedCdMapper;
import com.software.rateit.DTO.User.UserMapper;
import com.software.rateit.DTO.Wishlist.UserWishListDTO;
import com.software.rateit.DTO.Wishlist.WishListDTO;
import com.software.rateit.DTO.Wishlist.WishListMapper;
import com.software.rateit.Entity.*;
import com.software.rateit.exceptions.ForbiddenException;
import com.software.rateit.exceptions.NotFoundException;
import com.software.rateit.repositories.*;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class WishListServiceImpl implements WishListService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserWishListRepository userWishListRepository;

    @Autowired
    private WishListRepository wishListRepository;

    @Autowired
    private CDRepository cdRepository;

    @Autowired
    private CdTypeRepository cdTypeRepository;

    private WishListMapper wishListMapper = Mappers.getMapper(WishListMapper.class);

    private OwnedCdMapper ownedCdMapper = Mappers.getMapper(OwnedCdMapper.class);

    private UserMapper userMapper = Mappers.getMapper(UserMapper.class);

    @Override
    public ResponseEntity<WishListDTO> addToWishList(Authentication authentication, long cdId, Type type) {

        User user = userRepository.findOneByNickIgnoreCase(authentication.getPrincipal().toString());
        CD cd = cdRepository.findOneById(cdId);
        List<OwnedCd> friendsCds = new ArrayList<>();
        UserWishList userWishList = userWishListRepository.findOneByUser(user);

        if(cd == null) {
            throw new NotFoundException("Album not found");
        }

        if(!checkIfUrlIsReal(cd.getPhotoURL())){
            cd.setPhotoURL(getCover(cd.getPhotoURL()));
            cdRepository.save(cd);
        }

        CdType cdType = cdTypeRepository.findOneByType(type);
        if (cdType == null) {
            throw new NotFoundException("Type doesn't exist");
        }

        userWishList.getWishList().forEach(wish -> {
            if (wish.getCd().equals(cd) && wish.getCdType().equals(cdType)) {
                throw new ForbiddenException("Album already in users wishlist");
            }
        });

        user.getFriends().forEach(friend -> {
            friend.getUserscd().forEach(friendsCd -> {
                if(friendsCd.getCd().equals(cd) && friendsCd.getCdType().equals(cdType)){
                    friendsCds.add(friendsCd);
                }
            });
        });


        WishList wishList = new WishList();
        wishList.setCd(cd);
        wishList.setCdType(cdType);
        wishList.setFriendsAlbums(friendsCds);
        wishListRepository.save(wishList);


        userWishList.addToWishList(wishList);
        userWishListRepository.save(userWishList);

        return new ResponseEntity<>(wishListMapper.mapToWishListDTO(wishList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UserWishListDTO> getUsersWishList(long userId) {
        User user = userRepository.findOneById(userId);

        if(user == null) {
            throw new NotFoundException("User not found");
        }

        UserWishList userWishList = user.getUserWishList();

        UserWishListDTO userWishListDTO = new UserWishListDTO();
        userWishListDTO.setUser(userMapper.mapToUserDTO(user));
        userWishListDTO.setId(userWishList.getId());
        userWishListDTO.setWishList(wishListMapper.mapToWishListDTOList(userWishList.getWishList()));

        return new ResponseEntity<>(userWishListDTO, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Iterable<OwnedCdDTO>> getFriendsAlbumsFromWishList(Authentication authentication, long wishId) {
        User user = userRepository.findOneByNickIgnoreCase(authentication.getPrincipal().toString());
        WishList wishList = wishListRepository.findOneById(wishId);
        if(wishList == null) {
            throw new NotFoundException("Wish not found");
        }
        if(!user.getUserWishList().getWishList().contains(wishList)){
            throw new ForbiddenException("Wish doesn't belong to user");
        }

        return new ResponseEntity<>(ownedCdMapper.mapToCdDTOIterable(wishList.getFriendsAlbums()), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> deleteFromWishList(Authentication authentication, long wishId) {
        User user = userRepository.findOneByNickIgnoreCase(authentication.getPrincipal().toString());
        WishList wishList = wishListRepository.findOneById(wishId);
        if(wishList == null) {
            throw new NotFoundException("wish not found");
        }
        if(!user.getUserWishList().getWishList().contains(wishList)) {
            throw new ForbiddenException("Wish doesn't belong to user");
        }

        user.getUserWishList().removeFromWishList(wishList);
        userRepository.save(user);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    Boolean checkIfUrlIsReal(String response){
        String stringPattern = "http.*";
        Pattern pattern = Pattern.compile(stringPattern);
        Matcher matcher = pattern.matcher(response);
        if(matcher.find())
            return true;
        else
            return false;
    }
    String getRealImageUrl(String response){
        String stringPattern = "http.*(jpg|png|gif)";
        Pattern pattern = Pattern.compile(stringPattern);
        Matcher matcher = pattern.matcher(response);
        if(matcher.find())
            return matcher.group();
        else
            return "http://bobjames.com/wp-content/themes/soundcheck/images/default-album-artwork.png";
    }

    private String getCover( String link){
        String[] command = {"curl", link + "-250"};

        ProcessBuilder builder = new ProcessBuilder(command);
        builder.redirectErrorStream(true);

        String curlResult = "";
        String line = "";

        try {
            Process process = builder.start();
            BufferedReader r = new BufferedReader(new InputStreamReader(process.getInputStream()));
            while (true) {
                line = r.readLine();
                if (line == null) {
                    break;
                }
                curlResult = curlResult + line;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getRealImageUrl(curlResult );
    }

}
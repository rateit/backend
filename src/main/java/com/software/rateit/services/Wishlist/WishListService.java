package com.software.rateit.services.Wishlist;

import com.software.rateit.DTO.CD.OwnedCdDTO;
import com.software.rateit.DTO.Wishlist.UserWishListDTO;
import com.software.rateit.DTO.Wishlist.WishListDTO;
import com.software.rateit.Entity.Type;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface WishListService {

    ResponseEntity<WishListDTO> addToWishList(Authentication authentication, long cdId, Type type);
    ResponseEntity<UserWishListDTO> getUsersWishList(long userId);
    ResponseEntity<Iterable<OwnedCdDTO>> getFriendsAlbumsFromWishList(Authentication authentication, long wishId);
    ResponseEntity<Void> deleteFromWishList(Authentication authentication, long wishId);
}

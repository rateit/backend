package com.software.rateit.services.User;

import com.software.rateit.DTO.User.*;
import com.software.rateit.Entity.FriendRequest;
import com.software.rateit.Entity.User;
import com.software.rateit.Entity.UserWishList;
import com.software.rateit.Entity.WishList;
import com.software.rateit.exceptions.ForbiddenException;
import com.software.rateit.exceptions.NotFoundException;
import com.software.rateit.repositories.FriendRequestRepository;
import com.software.rateit.repositories.UserRepository;
import com.software.rateit.repositories.UserWishListRepository;
import com.software.rateit.repositories.WishListRepository;
import com.software.rateit.services.Email.EmailService;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class FriendsServiceImpl implements FriendsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FriendRequestRepository friendRequestRepository;

    @Autowired
    private WishListRepository wishListRepository;

    @Autowired
    private UserWishListRepository userWishListRepository;

    private FriendRequestMapper mapper = Mappers.getMapper(FriendRequestMapper.class);

    private UserMapper userMapper = Mappers.getMapper(UserMapper.class);

    @Autowired
    private EmailService emailService;

    @Override
    public ResponseEntity<Set<UserDTO>> getMyFriends(Authentication authentication) {
        User user = userRepository.findOneByNickIgnoreCase(authentication.getPrincipal().toString());

        Set<UserDTO> friendsSet = new HashSet<>();
        Iterable<UserDTO> friends = userMapper.mapToUserDTOIterable(user.getFriends());

        friends.forEach(friendsSet::add);

        return new ResponseEntity<>(friendsSet, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Set<UserDTO>> getFriendsByUserId(long id) {

        User user = userRepository.findOneById(id);

        if(user == null) {
            throw new NotFoundException("User not found");
        }

        Set<UserDTO> friendsSet = new HashSet<>();
        Iterable<UserDTO> friends = userMapper.mapToUserDTOIterable(user.getFriends());

        friends.forEach(friendsSet::add);

        return new ResponseEntity<>(friendsSet, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<FriendRequestDTO>> getAllRequests(Authentication authentication) {
        User user = userRepository.findOneByNickIgnoreCase(authentication.getPrincipal().toString());
        List<FriendRequest> friendRequestList = friendRequestRepository.findAllByReceiverId(user.getId());
        List<FriendRequestDTO> onlyNotAccepted = new ArrayList<>();

        friendRequestList.forEach(request -> {
            if(request.getStatus().equals("SENT")){
                FriendRequestDTO requestDto = mapper.mapToDto(request);
                User requester = userRepository.findOneById(request.getRequesterId());
                requestDto.setRequester(userMapper.mapToUserDTO(requester));
                onlyNotAccepted.add(requestDto);
            }
        });

        return new ResponseEntity<>(onlyNotAccepted, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<FriendRequestDTO> sendRequest(Authentication authentication, long receiverId) {
        User sender = userRepository.findOneByNickIgnoreCase(authentication.getPrincipal().toString());
        User receiver = userRepository.findOneById(receiverId);

        if (receiver == null){
            throw new NotFoundException("Receiver with given id does not exist");
        }

        if(sender.equals(receiver)){
            throw new ForbiddenException("You cannot add yourself to friends list");
        }

        FriendRequest request = friendRequestRepository.findOneByRequesterIdAndReceiverId(sender.getId(), receiver.getId());
        FriendRequest requestReverse = friendRequestRepository.findOneByRequesterIdAndReceiverId(receiver.getId(), sender.getId());
        FriendRequest newRequest;

        if(request == null && requestReverse == null){
            newRequest = new FriendRequest(sender.getId(), receiver.getId());
            friendRequestRepository.save(newRequest);
        } else {
            throw new  ForbiddenException("You have already sent a request");
        }

        FriendRequestDTO requestDTO = mapper.mapToDto(newRequest);
        requestDTO.setRequester(userMapper.mapToUserDTO(sender));

        if(!receiver.isActive()){
            emailService.sendFriendRequestNotification(receiver.getEmail(), sender.getNick());
        }

        return new ResponseEntity<>(requestDTO, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<FriendRequestDTO> replyRequest(Authentication authentication, long senderId, String status) {
        User receiver = userRepository.findOneByNickIgnoreCase(authentication.getPrincipal().toString());
        User sender = userRepository.findOneById(senderId);

        if(sender == null){
            throw new NotFoundException("Requester with given id does not exist");
        }

        FriendRequest request = friendRequestRepository.findOneByRequesterIdAndReceiverId(sender.getId(), receiver.getId());

        if(request == null) {
            throw new NotFoundException("Request does not exist");
        }

        if(request.getStatus().equals("ACCEPTED")){
            throw new ForbiddenException("This request is already accepted");
        }

        if(status.equals("ACCEPTED")){
            request.setStatus("ACCEPTED");
            friendRequestRepository.save(request);

            receiver.addFriend(sender);
            sender.addFriend(receiver);

            List<WishList> senderWishList = sender.getUserWishList().getWishList();
            List<WishList> receiverWishList = receiver.getUserWishList().getWishList();

            senderWishList.forEach(wishA -> {
                receiver.getUserscd().forEach( receivers-> {
                    if(receivers.getCdType().equals(wishA.getCdType()) && receivers.getCd().equals(wishA.getCd())) {
                        wishA.addToFriendsAlbums(receivers);
                        wishListRepository.save(wishA);
                    }
                });
            });

            receiverWishList.forEach(wishB -> {
                sender.getUserscd().forEach( senders-> {
                    if(senders.getCdType().equals(wishB.getCdType()) && senders.getCd().equals(wishB.getCd())) {
                        wishB.addToFriendsAlbums(senders);
                        wishListRepository.save(wishB);
                    }
                });
            });

            userRepository.save(receiver);
            userRepository.save(sender);
        } else if (status.equals("DENIED")) {
            request.setStatus("REQUEST DELETED");
            friendRequestRepository.delete(request);
        } else {
            throw new ForbiddenException("Wrong status");
        }

        FriendRequestDTO requestDTO = mapper.mapToDto(request);
        requestDTO.setRequester(userMapper.mapToUserDTO(sender));

        return new ResponseEntity<>(requestDTO, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<FriendRequestDTO> checkAsSeen(Authentication authentication, long senderId) {
        User sender = userRepository.findOneById(senderId);
        User receiver = userRepository.findOneByNickIgnoreCase(authentication.getPrincipal().toString());

        if(sender == null){
            throw new NotFoundException("Sender with given id does not exist");
        }

        FriendRequest request = friendRequestRepository.findOneByRequesterIdAndReceiverId(sender.getId(), receiver.getId());

        if(request == null) {
            throw new ForbiddenException("Request does not exist");
        }

        request.setSeen(true);

        friendRequestRepository.save(request);

        FriendRequestDTO requestDTO = mapper.mapToDto(request);
        requestDTO.setRequester(userMapper.mapToUserDTO(sender));

        return new ResponseEntity<>(requestDTO, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Set<UserPropositionDTO>> getFriendsByMutualFriends(Authentication authentication) {
        User user = userRepository.findOneByNickIgnoreCase(authentication.getPrincipal().toString());

        Set<User> myFriends = user.getFriends();
        List<User> allUsers = userRepository.findAll();
        Set<UserPropositionDTO> proposedFriends = new HashSet<>();

        Map<String, Integer> howManyMutualFriends = new HashMap<>();

        allUsers.forEach(stranger -> {
            if(!myFriends.contains(stranger) && !stranger.equals(user)) {
                myFriends.forEach(friend -> {
                    if(friend.getFriends().contains(stranger)){
                        if(howManyMutualFriends.containsKey(stranger.getNick())){
                            int howMany = howManyMutualFriends.get(stranger.getNick()) + 1;
                            howManyMutualFriends.put(stranger.getNick(), howMany);
                        } else {
                            howManyMutualFriends.put(stranger.getNick(), 1);
                        }

                    }
                });
            }
        });

        howManyMutualFriends.entrySet().stream()
                .sorted((k1, k2) -> -k1.getValue().compareTo(k2.getValue()))
                .forEach(k -> {
                    UserPropositionDTO userProposition = new UserPropositionDTO();
                    User proposition = userRepository.findOneByNickIgnoreCase(k.getKey());
                    userProposition.setHowManyMutualFriends(k.getValue());
                    userProposition.setNick(proposition.getNick());
                    userProposition.setId(proposition.getId());
                    userProposition.setPhotoURL(proposition.getPhotoURL());
                    proposedFriends.add(userProposition);
                });

        List<UserPropositionDTO> propositions = new ArrayList<>(proposedFriends);

        Set<UserPropositionDTO> response = new HashSet<>();

        if(proposedFriends.size() > 6) {
            for(int i = 0 ; i < 5 ; i++) {
                response.add(propositions.get(i));
            }
        } else {
            for(int i = 0 ; i < proposedFriends.size() ; i++){
                response.add(propositions.get(i));
            }
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> deleteFriend(Authentication authentication, long friendId) {
        User user = userRepository.findOneByNickIgnoreCase(authentication.getPrincipal().toString());
        User friend = userRepository.findOneById(friendId);

        if(friend == null) {
            throw new NotFoundException("User not found");
        }

        if(!user.getFriends().contains(friend) || !friend.getFriends().contains(user)) {
            throw  new ForbiddenException("Relation doesn't exist");
        }

        user.removeFriend(friend);
        friend.removeFriend(user);

        FriendRequest byFriend = friendRequestRepository.findOneByRequesterIdAndReceiverId(friend.getId(), user.getId());
        FriendRequest byUser = friendRequestRepository.findOneByRequesterIdAndReceiverId(user.getId(), friend.getId());

        if(byFriend != null){
            friendRequestRepository.delete(byFriend);
        }
        if(byUser != null){
            friendRequestRepository.delete(byUser);
        }

        userRepository.save(user);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}

package com.software.rateit.services.User;

import com.software.rateit.DTO.CD.OwnedCdDTO;
import com.software.rateit.DTO.Comments.CommentAlbumDTO;
import com.software.rateit.DTO.Comments.CommentsDTO;
import com.software.rateit.DTO.Rate.RateDTO;
import com.software.rateit.DTO.User.*;
import com.software.rateit.Entity.Type;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

public interface UserService {
    ResponseEntity<UserDTO> getOneById(long id, Authentication authentication);
    ResponseEntity<UserResponseWrapper> listOfUsers(Pageable pageable);
    ResponseEntity<UserDTO> findByEmail(String email);
    ResponseEntity<Iterable<UserDTO>> findByNick(String nick);
    ResponseEntity<UserDTO> registerNewUser(RegistrationDTO registrationDTO);
    ResponseEntity<UserDTO> changePassword(long id, PasswordChangeDTO passwordChangeDTO);
    ResponseEntity<UserDTO> addCdToUser(long userId, long cdId, Type type);
    ResponseEntity<Void> removeCdFromUser(long ownedCdId, Authentication authentication);
    ResponseEntity<OwnedCdDTO> giveOwnedCdToAnotherUser(Authentication authentication, long cdId, long userId);
    ResponseEntity<Iterable<OwnedCdDTO>> getUsersCds(long id);
    ResponseEntity<Void> deleteUser(long id);
    ResponseEntity<CommentsDTO> commentAlbum(CommentAlbumDTO comment, long id);
    ResponseEntity<UserDTO> setAsAdmin(long id);
    ResponseEntity<Void> logout(long id);
    ResponseEntity<UserDTO> uploadAvatar(MultipartFile multipartFile, long id);
    ResponseEntity<CommentsDTO> editComment(CommentAlbumDTO comment, long id);
    ResponseEntity<Void> deleteComment(long commentId, long userId);
    ResponseEntity<InputStreamResource> generateRaport(int days);
    ResponseEntity<List<RateDTO>> getRatedAlbums(long userId);
    ResponseEntity<UserDTO> changeEmail(ChangeEmailDTO changeEmailDTO, long id);
}

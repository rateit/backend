package com.software.rateit.services.User;

public enum RequestStatus {
    SENT,
    NOT_SENT,
    RESPONSE_TO_REQUEST,
    ACCEPTED
}

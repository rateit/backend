package com.software.rateit.services.User;

import com.software.rateit.DTO.User.FriendRequestDTO;
import com.software.rateit.DTO.User.UserDTO;
import com.software.rateit.DTO.User.UserPropositionDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;

import java.util.List;
import java.util.Set;

public interface FriendsService {
    ResponseEntity<Set<UserDTO>> getMyFriends(Authentication authentication);
    ResponseEntity<Set<UserDTO>> getFriendsByUserId(long id);
    ResponseEntity<List<FriendRequestDTO>> getAllRequests(Authentication authentication);
    ResponseEntity<FriendRequestDTO> sendRequest(Authentication authentication, long receiverId);
    ResponseEntity<FriendRequestDTO> replyRequest(Authentication authentication, long senderId, String status);
    ResponseEntity<FriendRequestDTO> checkAsSeen(Authentication authentication, long senderId);
    ResponseEntity<Set<UserPropositionDTO>> getFriendsByMutualFriends(Authentication authentication);
    ResponseEntity<Void> deleteFriend(Authentication authentication, long friendId);

}

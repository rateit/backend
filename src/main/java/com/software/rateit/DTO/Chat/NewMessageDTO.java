package com.software.rateit.DTO.Chat;

public class NewMessageDTO {
    private long receiverId;
    private String message;

    public Long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(long receiverId) {
        this.receiverId = receiverId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

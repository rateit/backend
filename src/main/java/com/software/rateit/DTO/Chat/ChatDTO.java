package com.software.rateit.DTO.Chat;

import com.fasterxml.jackson.annotation.JsonView;
import com.software.rateit.DTO.User.UserDTO;
import com.software.rateit.DTO.View;

import java.util.List;

public class ChatDTO {

    @JsonView(View.Summary.class)
    private long id;
    private List<MessageDTO> messages;
    @JsonView(View.Summary.class)
    private UserDTO me;
    @JsonView(View.Summary.class)
    private UserDTO participant;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<MessageDTO> getMessages() {
        return messages;
    }

    public void setMessages(List<MessageDTO> messages) {
        this.messages = messages;
    }

    public UserDTO getMe() {
        return me;
    }

    public void setMe(UserDTO me) {
        this.me = me;
    }

    public UserDTO getParticipant() {
        return participant;
    }

    public void setParticipant(UserDTO participant) {
        this.participant = participant;
    }
}

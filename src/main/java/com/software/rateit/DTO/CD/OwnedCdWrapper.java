package com.software.rateit.DTO.CD;

import com.fasterxml.jackson.annotation.JsonView;
import com.software.rateit.DTO.PaginationContext;
import com.software.rateit.DTO.View;

@JsonView(View.Summary.class)
public class OwnedCdWrapper {

    private Iterable<OwnedCdDTO> cds;
    private PaginationContext context;

    public OwnedCdWrapper(Iterable<OwnedCdDTO> cds, PaginationContext context) {
        this.cds = cds;
        this.context = context;
    }

    public Iterable<OwnedCdDTO> getCds() {
        return cds;
    }

    public void setCds(Iterable<OwnedCdDTO> cds) {
        this.cds = cds;
    }

    public PaginationContext getContext() {
        return context;
    }

    public void setContext(PaginationContext context) {
        this.context = context;
    }
}

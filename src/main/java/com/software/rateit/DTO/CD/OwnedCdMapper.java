package com.software.rateit.DTO.CD;

import com.software.rateit.Entity.CdType;
import com.software.rateit.Entity.OwnedCd;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper
public interface OwnedCdMapper {

    @Named("noCollections")
    @Mapping(ignore = true, target = "cds")
    CdTypeDTO mapToCdTypeDtoWithoutCds(CdType cdType);

    @Named("no")
    @Mapping(ignore = true, target = "cdType.cds")
    @Mapping(ignore = true, target = "user.userscd")
    @Mapping(ignore = true, target = "user.comments")
    @Mapping(ignore = true, target = "cd.cdtracks")
    @Mapping(ignore = true, target = "cd.user")
    @Mapping(ignore = true, target = "cd.comments")
    OwnedCdDTO mapToOwnedCdDto(OwnedCd ownedCd);

    @IterableMapping(qualifiedByName = "no")
    Iterable<OwnedCdDTO> mapToCdDTOIterable(Iterable<OwnedCd> cds);

}

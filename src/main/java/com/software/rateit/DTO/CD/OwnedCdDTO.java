package com.software.rateit.DTO.CD;

import com.fasterxml.jackson.annotation.JsonView;
import com.software.rateit.DTO.User.UserDTO;
import com.software.rateit.DTO.View;

import java.util.Set;


@JsonView(View.Summary.class)
public class OwnedCdDTO {

    private long id;
    private UserDTO user;
    private CdDTO cd;
    private CdTypeDTO cdType;
    private Set<PhotoDTO> photos;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public CdDTO getCd() {
        return cd;
    }

    public void setCd(CdDTO cd) {
        this.cd = cd;
    }

    public CdTypeDTO getCdType() {
        return cdType;
    }

    public void setCdType(CdTypeDTO cdType) {
        this.cdType = cdType;
    }

    public Set<PhotoDTO> getPhotos() {
        return photos;
    }

    public void setPhotos(Set<PhotoDTO> photos) {
        this.photos = photos;
    }
}

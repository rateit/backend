package com.software.rateit.DTO.CD;

import com.fasterxml.jackson.annotation.JsonView;
import com.software.rateit.DTO.View;
import com.software.rateit.Entity.Type;

import java.util.List;


public class CdTypeDTO {

    private long id;
    @JsonView(View.Summary.class)
    private Type type;
    private List<OwnedCdDTO> cds;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public List<OwnedCdDTO> getCds() {
        return cds;
    }

    public void setCds(List<OwnedCdDTO> cds) {
        this.cds = cds;
    }
}

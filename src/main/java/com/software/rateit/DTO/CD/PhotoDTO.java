package com.software.rateit.DTO.CD;

import com.fasterxml.jackson.annotation.JsonView;
import com.software.rateit.DTO.View;

@JsonView(View.Summary.class)
public class PhotoDTO {
    private long id;
    private String photoUrl;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}

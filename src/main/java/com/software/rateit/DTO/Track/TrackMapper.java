package com.software.rateit.DTO.Track;

import com.software.rateit.Entity.Track;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper
public interface TrackMapper {

    TrackDTO mapToTrackDTO(Track track);

    Iterable<TrackDTO> mapToTrackDTOIterable(Iterable<Track> tracks);

    Track mapToTrack(TrackDTO trackDTO);
}

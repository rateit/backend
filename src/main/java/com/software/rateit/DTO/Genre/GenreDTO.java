package com.software.rateit.DTO.Genre;

import com.fasterxml.jackson.annotation.JsonView;
import com.software.rateit.DTO.View;

public class GenreDTO {

    private long id;
    @JsonView({View.Summary.class, View.Comment.class})
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

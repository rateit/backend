package com.software.rateit.DTO.Genre;

import com.software.rateit.Entity.Genre;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface GenreMapper {

    GenreDTO mapToGenreDTO(Genre genre);
    List<GenreDTO> mapToGenreDTOList(List<Genre> genres);

}

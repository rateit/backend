package com.software.rateit.DTO.User;

import com.software.rateit.Entity.FriendRequest;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface FriendRequestMapper {

    FriendRequestDTO mapToDto(FriendRequest friendRequest);
    List<FriendRequestDTO> mapListToDto(List<FriendRequest> friendRequestList);
}

package com.software.rateit.DTO.User;

public class UserPropositionDTO {
    private Long id;
    private String nick;
    private String photoURL;
    private int howManyMutualFriends;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public int getHowManyMutualFriends() {
        return howManyMutualFriends;
    }

    public void setHowManyMutualFriends(int howManyMutualFriends) {
        this.howManyMutualFriends = howManyMutualFriends;
    }
}

package com.software.rateit.DTO.Wishlist;

import com.software.rateit.DTO.User.UserDTO;
import java.util.List;

public class UserWishListDTO {

    private long id;
    private UserDTO user;
    private List<WishListDTO> wishList;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public List<WishListDTO> getWishList() {
        return wishList;
    }

    public void setWishList(List<WishListDTO> wishList) {
        this.wishList = wishList;
    }
}

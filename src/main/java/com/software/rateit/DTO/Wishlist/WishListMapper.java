package com.software.rateit.DTO.Wishlist;

import com.software.rateit.Entity.WishList;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper
public interface WishListMapper {

    @Named("no")
    @Mapping(ignore = true, target = "cdType.cds")
    @Mapping(ignore = true, target = "cd.cdtracks")
    @Mapping(ignore = true, target = "cd.comments")
    @Mapping(ignore = true, target = "cd.user")
    @Mapping(ignore = true, target = "friendsAlbums")
    WishListDTO mapToWishListDTO(WishList wishList);

    @IterableMapping(qualifiedByName = "no")
    List<WishListDTO> mapToWishListDTOList(List<WishList> wishLists);
}

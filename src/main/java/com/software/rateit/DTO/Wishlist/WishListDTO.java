package com.software.rateit.DTO.Wishlist;

import com.software.rateit.DTO.CD.CdDTO;
import com.software.rateit.DTO.CD.CdTypeDTO;
import com.software.rateit.DTO.CD.OwnedCdDTO;
import com.software.rateit.DTO.User.UserDTO;

import java.util.List;

public class WishListDTO {

    private long id;
    private CdDTO cd;
    private CdTypeDTO cdType;
    private List<OwnedCdDTO> friendsAlbums;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public CdDTO getCd() {
        return cd;
    }

    public void setCd(CdDTO cd) {
        this.cd = cd;
    }

    public CdTypeDTO getCdType() {
        return cdType;
    }

    public void setCdType(CdTypeDTO cdType) {
        this.cdType = cdType;
    }

    public List<OwnedCdDTO> getFriendsAlbums() {
        return friendsAlbums;
    }

    public void setFriendsAlbums(List<OwnedCdDTO> friendsAlbums) {
        this.friendsAlbums = friendsAlbums;
    }
}

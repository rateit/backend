package com.software.rateit.JWT;

import com.auth0.jwt.JWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.software.rateit.DTO.User.LoginDTO;
import com.software.rateit.DTO.User.UserDTO;
import com.software.rateit.DTO.User.UserMapper;
import com.software.rateit.Entity.User;
import com.software.rateit.exceptions.ForbiddenException;
import com.software.rateit.repositories.UserRepository;
import org.mapstruct.factory.Mappers;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private UserRepository userRepository;

    private AuthenticationManager authenticationManager;

    private UserMapper mapper = Mappers.getMapper(UserMapper.class);

    public JwtAuthenticationFilter(AuthenticationManager authenticationManager, UserRepository userRepository) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {

        LoginDTO credentials = null;
        try {
            credentials = new ObjectMapper().readValue(request.getInputStream(), LoginDTO.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                credentials.getUsername(),
                credentials.getPassword(),
                new ArrayList<>());

        Authentication auth = authenticationManager.authenticate(authenticationToken);

        return auth;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        UserDetailsImpl principal = (UserDetailsImpl) authResult.getPrincipal();

        User user = userRepository.findOneByNickIgnoreCase(principal.getUsername());

        if(user == null) {
            throw new ForbiddenException("Username not found");
        }

        String token = JWT.create()
                .withSubject(principal.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + JwtProps.EXPIRATION_TIME))
                .sign(HMAC512(JwtProps.SECRET.getBytes()));

        response.addHeader("Content-Type", "application/json");

        user.setActive(true);
        userRepository.save(user);
        UserDTO userDTO = mapper.mapToUserDTO(user);
        userDTO.setAuthToken(JwtProps.TOKEN_PREFIX +  token);
        response.setStatus(HttpStatus.OK.value());
        String json = new ObjectMapper().writeValueAsString(userDTO);
        response.getWriter().write(json);
        response.flushBuffer();
    }
}
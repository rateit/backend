package com.software.rateit.JWT;

public class JwtProps {
    public static final String SECRET = "RateItApplicationSecret";
    public static final int EXPIRATION_TIME = 86400000;
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
}

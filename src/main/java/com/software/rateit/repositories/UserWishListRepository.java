package com.software.rateit.repositories;

import com.software.rateit.Entity.User;
import com.software.rateit.Entity.UserWishList;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserWishListRepository extends JpaRepository<UserWishList, Long> {
    UserWishList findOneById(long id);
    UserWishList findOneByUser(User user);
}

package com.software.rateit.repositories;

import com.software.rateit.Entity.Photos;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhotosRepository extends JpaRepository<Photos, Long > {
    Photos findOneById(long id);
}

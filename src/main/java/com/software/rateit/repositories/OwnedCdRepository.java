package com.software.rateit.repositories;

import com.querydsl.core.types.dsl.StringPath;
import com.software.rateit.Entity.OwnedCd;
import com.software.rateit.Entity.QOwnedCd;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface OwnedCdRepository extends JpaRepository<OwnedCd, Long> , QuerydslPredicateExecutor<OwnedCd>,
        QuerydslBinderCustomizer<QOwnedCd> {
    OwnedCd findOneById(long id);

    @Override
    default void customize(QuerydslBindings bindings, QOwnedCd root){
        bindings.bind(String.class).first(
                (StringPath path, String value) -> path.containsIgnoreCase(value));
        bindings.bind(root.cd.released).all(((path, value) -> {
            List<? extends Integer> dates = new ArrayList<>(value);
            if (dates.size() == 1) {
                return Optional.of(path.eq(dates.get(0)));
            } else {
                Integer from = dates.get(0);
                Integer to = dates.get(1);
                return Optional.of(path.between(from, to));
            }
        }));
    }
}

package com.software.rateit.repositories;

import com.software.rateit.Entity.FriendRequest;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FriendRequestRepository extends JpaRepository<FriendRequest, Long> {
    List<FriendRequest> findAllByRequesterId(long id);
    List<FriendRequest> findAllByReceiverId(long id);
    FriendRequest findOneByRequesterIdAndReceiverId(long requesterId, long receiverId);
}

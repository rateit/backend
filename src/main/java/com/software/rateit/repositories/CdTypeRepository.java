package com.software.rateit.repositories;

import com.software.rateit.Entity.CdType;
import com.software.rateit.Entity.Type;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CdTypeRepository extends JpaRepository<CdType, Long> {
    CdType findOneByType(Type type);
}

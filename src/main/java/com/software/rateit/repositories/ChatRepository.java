package com.software.rateit.repositories;

import com.software.rateit.Entity.Chat;
import com.software.rateit.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface ChatRepository extends JpaRepository<Chat, Long> {
    Set<Chat> findAllByParticipant1OrParticipant2(User participant1, User participant2);
    Chat findOneByParticipant1AndParticipant2(User participant1, User participant2);
}

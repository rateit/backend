package com.software.rateit.repositories;

import com.software.rateit.Entity.CD;
import com.software.rateit.Entity.CdType;
import com.software.rateit.Entity.WishList;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WishListRepository extends JpaRepository<WishList, Long> {
    WishList findOneById(long id);
    WishList findOneByCdAndCdType(CD cd, CdType cdType);
}

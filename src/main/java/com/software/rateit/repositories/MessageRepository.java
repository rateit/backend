package com.software.rateit.repositories;

import com.software.rateit.Entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<Message, Long> {
    Message findOneById(long id);
}

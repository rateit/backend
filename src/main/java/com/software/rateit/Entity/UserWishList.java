package com.software.rateit.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class UserWishList {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne( fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<WishList> wishList;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<WishList> getWishList() {
        return wishList;
    }

    public void setWishList(List<WishList> wishList) {
        this.wishList = wishList;
    }

    public void addToWishList(WishList wishList){
        this.getWishList().add(wishList);
    }

    public void removeFromWishList(WishList wishList) {
        this.getWishList().remove(wishList);
    }
}

package com.software.rateit.Entity;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "CD")
public class CD {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(columnDefinition = "TEXT")
    private String name;
    @JsonIgnore
    private long releaseId;
    private Integer released;
    private float rating = 0;
    private String comment;
    private int ratingCount = 0;
    private float sumOfRating = 0;
    @JsonIgnore
    private String gid;
    private String photoURL = "http://bobjames.com/wp-content/themes/soundcheck/images/default-album-artwork.png";
    @Column(columnDefinition = "TEXT")
    private String artist;
    @JsonManagedReference
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Track> cdtracks;
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Genre> genre;

    @JsonBackReference
    @OneToMany(mappedBy = "cd", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Comments> comments;

    public CD(){}

    public CD(String name, int released, String comment, int ratingCount, int sumOfRating, String photoURL, String artist, List<Track> track) {
        this.name = name;
        this.released = released;
        this.comment = comment;
        this.cdtracks = track;
        this.artist = artist;
        this.ratingCount = ratingCount;
        this.sumOfRating = sumOfRating;
        this.photoURL = photoURL;
    }

    public long getReleaseId() {
        return releaseId;
    }

    public void setReleaseId(long releaseId) {
        this.releaseId = releaseId;
    }

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getReleased() {
        return released;
    }

    public void setReleased(int released) {
        this.released = released;
    }
    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
    public String getArtist() {
        return artist;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public List<Track> getCdtracks() {
        return cdtracks;
    }

    public void setCdtracks(List<Track> cdtracks) {
        this.cdtracks = cdtracks;
    }

    public void setReleased(Integer released) {
        this.released = released;
    }

    public List<Genre> getGenre() {
        return genre;
    }

    public void setGenre(List<Genre> genre) {
        this.genre = genre;
    }

    public int getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(int ratingCount) {
        this.ratingCount = ratingCount;
    }

    public float getSumOfRating() {
        return sumOfRating;
    }

    public void setSumOfRating(float sumOfRating) {
        this.sumOfRating = sumOfRating;
    }

    public String getPhotoURL() { return photoURL; }

    public void setPhotoURL(String photoURL) { this.photoURL = photoURL; }

    public List<Comments> getComments() {
        return comments;
    }

    public void setComments(List<Comments> comments) {
        this.comments = comments;
    }

    public void addTrack(Track track) {
        this.getCdtracks().add(track);
    }

}

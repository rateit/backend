package com.software.rateit.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class CdType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Enumerated(EnumType.STRING)
    private Type type;
    @OneToMany(mappedBy = "cd", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<OwnedCd> cds;

    public CdType(){}
    public CdType(Type type){
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public List<OwnedCd> getCds() {
        return cds;
    }

    public void addCd(OwnedCd cd) {
        this.getCds().add(cd);
    }
}

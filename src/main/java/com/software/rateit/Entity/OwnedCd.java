package com.software.rateit.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class OwnedCd {

    @Id
    @GeneratedValue
    private long id;
    @JsonBackReference
    @ManyToOne( fetch = FetchType.LAZY)
    private User user;
    @ManyToOne(fetch = FetchType.LAZY)
    private CD cd;
    @ManyToOne(fetch = FetchType.LAZY)
    private CdType cdType;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Photos> photos = new HashSet<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public CD getCd() {
        return cd;
    }

    public void setCd(CD cd) {
        this.cd = cd;
    }

    public CdType getCdType() {
        return cdType;
    }

    public void setCdType(CdType cdType) {
        this.cdType = cdType;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Photos> getPhoto() {
        return photos;
    }

    public void setPhoto(Set<Photos> photo) {
        this.photos = photo;
    }

    public void addPhoto(Photos photo){
        this.getPhoto().add(photo);
    }

    public void removePhoto(Photos photo){
        this.getPhoto().remove(photo);
    }
}

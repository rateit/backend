package com.software.rateit.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class WishList {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne(fetch = FetchType.LAZY)
    private CD cd;
    @ManyToOne(fetch = FetchType.LAZY)
    private CdType cdType;
    @ManyToMany(fetch = FetchType.LAZY)
    private List<OwnedCd> friendsAlbums;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public CD getCd() {
        return cd;
    }

    public void setCd(CD cd) {
        this.cd = cd;
    }

    public CdType getCdType() {
        return cdType;
    }

    public void setCdType(CdType cdType) {
        this.cdType = cdType;
    }

    public List<OwnedCd> getFriendsAlbums() {
        return friendsAlbums;
    }

    public void setFriendsAlbums(List<OwnedCd> friendsAlbums) {
        this.friendsAlbums = friendsAlbums;
    }

    public void addToFriendsAlbums(OwnedCd ownedCd){
        this.getFriendsAlbums().add(ownedCd);
    }

    public void removeFromFriendsAlbums(OwnedCd ownedCd){
        this.getFriendsAlbums().remove(ownedCd);
    }
}

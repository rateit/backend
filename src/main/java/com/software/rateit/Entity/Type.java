package com.software.rateit.Entity;

public enum Type {
    vinyl,
    cd,
    cassette,
    other
}

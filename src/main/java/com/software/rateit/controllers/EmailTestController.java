package com.software.rateit.controllers;

import com.software.rateit.DTO.Email.EmailInfo;
import com.software.rateit.services.Email.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailTestController {

    @Autowired
    private EmailService emailService;

    @PostMapping("/send-email")
    public ResponseEntity<String> sendTestMail(@RequestBody EmailInfo emailInfo){
        return emailService.sendSimpleEmail(emailInfo);
    }


}

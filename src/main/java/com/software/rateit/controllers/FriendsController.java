package com.software.rateit.controllers;

import com.software.rateit.DTO.User.FriendRequestDTO;
import com.software.rateit.DTO.User.UserDTO;
import com.software.rateit.DTO.User.UserPropositionDTO;
import com.software.rateit.services.User.FriendsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class FriendsController {

    @Autowired
    private FriendsService friendsService;

    @GetMapping("/users/my-friends")
    public ResponseEntity<Set<UserDTO>> getMyFriendsList(){
        return friendsService.getMyFriends(SecurityContextHolder.getContext().getAuthentication());
    }

    @GetMapping("/users/my-requests")
    public ResponseEntity<List<FriendRequestDTO>> getMyRequests(){
        return friendsService.getAllRequests(SecurityContextHolder.getContext().getAuthentication());
    }

    @GetMapping("/users/friendsPropositions")
    public ResponseEntity<Set<UserPropositionDTO>> getByMutualFriends(){
        return friendsService.getFriendsByMutualFriends(SecurityContextHolder.getContext().getAuthentication());
    }

    @GetMapping("/users/{id}/friends")
    public ResponseEntity<Set<UserDTO>> getUserFriends(@PathVariable long id){
        return friendsService.getFriendsByUserId(id);
    }

    @PostMapping("/users/{receiverId}/send-request")
    public ResponseEntity<FriendRequestDTO> sendRequest(@PathVariable long receiverId){
        return friendsService.sendRequest(SecurityContextHolder.getContext().getAuthentication(), receiverId);
    }

    @PostMapping("/requests/{requesterId}/reply")
    public ResponseEntity<FriendRequestDTO> replyToRequest(@PathVariable long requesterId, @RequestParam String status){
        return friendsService.replyRequest(SecurityContextHolder.getContext().getAuthentication(), requesterId, status);
    }

    @PostMapping("/request/{requesterId}/seen")
    public ResponseEntity<FriendRequestDTO> checkAsSeen(@PathVariable long requesterId){
        return friendsService.checkAsSeen(SecurityContextHolder.getContext().getAuthentication(), requesterId);
    }

    @DeleteMapping("/friends/{friendId}")
    public ResponseEntity<Void> deleteFriend(@PathVariable long friendId) {
        return friendsService.deleteFriend(SecurityContextHolder.getContext().getAuthentication(), friendId);
    }
}

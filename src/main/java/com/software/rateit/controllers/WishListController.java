package com.software.rateit.controllers;

import com.software.rateit.DTO.CD.OwnedCdDTO;
import com.software.rateit.DTO.Wishlist.UserWishListDTO;
import com.software.rateit.DTO.Wishlist.WishListDTO;
import com.software.rateit.Entity.Type;
import com.software.rateit.services.Wishlist.WishListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
public class WishListController {

    @Autowired
    private WishListService wishListService;

    @GetMapping("/users/{id}/wish-list")
    public ResponseEntity<UserWishListDTO> getUsersWishList(@PathVariable long id){
        return wishListService.getUsersWishList(id);
    }

    @GetMapping("/wish-list/{wish-id}/friends-cds")
    public ResponseEntity<Iterable<OwnedCdDTO>> getFriendsCdFromWishList(@PathVariable(name = "wish-id") long id) {
        return wishListService.getFriendsAlbumsFromWishList(SecurityContextHolder.getContext().getAuthentication(), id);
    }

    @PostMapping("/wish-list")
    public ResponseEntity<WishListDTO> addToWishList(@RequestParam long cdId, @RequestParam Type type){
        return wishListService.addToWishList(SecurityContextHolder.getContext().getAuthentication(), cdId, type);
    }

    @DeleteMapping("/wish-list/{wish-id}")
    public ResponseEntity<Void> removeFromWishList(@PathVariable(name = "wish-id") long id) {
        return wishListService.deleteFromWishList(SecurityContextHolder.getContext().getAuthentication(), id);
    }


}

package com.software.rateit.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.software.rateit.DTO.Chat.ChatDTO;
import com.software.rateit.DTO.Chat.NewMessageDTO;
import com.software.rateit.DTO.View;
import com.software.rateit.services.Chat.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ChatController {

    @Autowired
    private ChatService chatService;

    @JsonView(View.Summary.class)
    @GetMapping("/chats")
    public ResponseEntity<List<ChatDTO>> getMyChats(){
        return chatService.getMyChats(SecurityContextHolder.getContext().getAuthentication());
    }

    @PostMapping("/chats")
    public ResponseEntity<ChatDTO> newChat(@RequestParam(name = "personId") long secondPersonId){
        return chatService.newChat(SecurityContextHolder.getContext().getAuthentication(), secondPersonId);
    }

    @GetMapping("/chats/{personId}")
    public ResponseEntity<ChatDTO> getOneChat(@PathVariable long personId){
        return chatService.getOneChat(SecurityContextHolder.getContext().getAuthentication(), personId);
    }

    @PostMapping("/chats/newMessage")
    public ResponseEntity<ChatDTO> writeNewMessage(@RequestBody NewMessageDTO newMessageDTO){
        return chatService.writeNewMessage(SecurityContextHolder.getContext().getAuthentication(), newMessageDTO);
    }

    @PostMapping("/chats/check-as-seen")
    ResponseEntity<ChatDTO> checkMessageAsSeen(@RequestParam(name = "personId") long secondPersonId ){
        return chatService.checkMessageAsSeen(SecurityContextHolder.getContext().getAuthentication(), secondPersonId);

    }

}

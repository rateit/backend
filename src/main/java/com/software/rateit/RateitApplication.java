package com.software.rateit;

import com.software.rateit.Entity.CdType;
import com.software.rateit.Entity.Type;
import com.software.rateit.repositories.CdTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RateitApplication implements CommandLineRunner {

    public static void main(String[] args) { SpringApplication.run(RateitApplication.class, args); }

    @Autowired
    CdTypeRepository cdTypeRepository;

    @Override
    public void run(String...args) throws Exception {
        CdType vinyl = cdTypeRepository.findOneByType(Type.vinyl);
        CdType cd = cdTypeRepository.findOneByType(Type.cd);
        CdType cassette = cdTypeRepository.findOneByType(Type.cassette);
        CdType other = cdTypeRepository.findOneByType(Type.other);

        if(vinyl == null && cd == null && cassette == null && other == null) {
            vinyl = new CdType(Type.vinyl);
            cd = new CdType(Type.cd);
            cassette = new CdType(Type.cassette);
            other = new CdType(Type.other);
            cdTypeRepository.save(vinyl);
            cdTypeRepository.save(cd);
            cdTypeRepository.save(cassette);
            cdTypeRepository.save(other);
        }

    }

}
package com.software.rateit.Listener;

import com.software.rateit.DTO.Email.EmailInfo;
import com.software.rateit.services.Email.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class EmailListener {

    @Autowired
    EmailService emailService;

    @JmsListener(destination = "email-queue")
    public void listener(Map<String, String> mappedEmail){
        EmailInfo emailInfo = new EmailInfo();
        emailInfo.setContent(mappedEmail.get("message"));
        emailInfo.setSubject(mappedEmail.get("subject"));
        emailInfo.setTo(mappedEmail.get("to"));
        emailService.simpleEmail(emailInfo);
    }

}
